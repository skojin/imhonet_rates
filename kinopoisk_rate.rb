require 'watir'
require 'nokogiri'

SESSION_COOKIE = ''
BACKUP_DIR = '' # r

trap('TERM') { exit }
trap('INT') { exit }

def downloaded_export_by_name(name)
  @exported_items ||= begin
    doc=Nokogiri::HTML(File.read('export/imhonet-rates-films.htm'))
    doc.search('tr').select{|tr| tr.at('a')}.map{|tr| {url: tr.at('a')[:href], name: tr.at('a').text, rate: tr.at('td:last-child').text} }.select{|it| it[:url] =~ /kinopoisk/ }
  end
  names = @exported_items.select{|d| d[:name] == name }
  names.size == 1 && names.first
end


def browser
  $browser ||= create_browser
end

def create_browser
  browser = Watir::Browser.new :chrome

  browser.goto 'https://www.kinopoisk.ru'
  browser.cookies.clear
  browser.cookies.add 'Session_id', SESSION_COOKIE
  browser.goto 'https://plus.kinopoisk.ru'
  browser.cookies.clear
  browser.cookies.add 'Session_id', SESSION_COOKIE
  puts 'browser loaded'
  browser
end

$_db = JSON.load(File.read('.db-kinopoisk-rate.json')) rescue {}
def mark_as_exported(item, url)
  $_db[item['object_id']] = {title: item['title'], url: url, rate: item['rate']}
  File.write('.db-kinopoisk-rate.json', JSON.dump($_db))
end
def already_exported?(item)
  $_db[item['object_id'].to_s]
end

def export_item(item)
  kinopoisk_hash = downloaded_export_by_name(item['title'])
  if kinopoisk_hash && kinopoisk_hash[:rate].to_s == item['rate'].to_s
    puts "  found in downloaded export #{kinopoisk_hash[:url]}"
    rate!(item, kinopoisk_hash[:url])
    return
  end


  title = item['title']
  if title =~ /(.*?)\s\(\d{4}(-\d{4})?\)$/
    title = $1
    puts "  #{title}"
  end
  browser.goto 'https://plus.kinopoisk.ru'
  browser.text_field(:class => "input__input", name: 'text').set title
  found = browser.div(class: 'input__suggest_visible')
  sleep 2
  if !found.exist?
    puts '  Not Found'
    `say not found`;puts 'press any key';$stdin.gets;$ask = true
    return
  end
  links = found&.links.select{|x| x.href =~ /film/ }
  if links.size == 0
    puts '  Not Found'
    `say not found`;puts 'press any key';$stdin.gets;$ask = true
    return
  end

  if links.size > 1 # try reduce by year
    less_links = links.select{|link| link.element(class: 'input__suggest-content').text =~ /#{item['year']}$/ }
    links = less_links if less_links.size == 1
  end

  a = links.first
  if links.size > 1
    # puts "  #{links.size} items found, skip"; return
    links.each_with_index do |link, i|
      puts "  #{i + 1}: #{link.element(class: 'input__suggest-content').text}  #{link.href}"
    end
    $ask = true
    puts 'SELECT?'
    `say select`
    n = $stdin.gets.chomp.to_i
    if n == 0
      puts '  skip'
      return
    end
    a = links[n - 1]
  end
  url = a.href
  puts "  #{url}"
  data = JSON.load a.attribute_value('data-bem')
  if data['input__suggest-item']['value'] != title
    $ask = true
    puts "  Title not match: #{data['input__suggest-item']['value']} != #{title}, that's ok?"
    puts a.element(class: 'input__suggest-content').text
    `say confirm`
    if $stdin.gets.chomp != 'y'
      return
    end
  end
  rate!(item, url)
end

def rate!(item, url)
  url.gsub!('plus.', 'www.')
  browser.goto url
  browser.div(class: 'starbar').a(class: "s#{item['rate']}").click
  mark_as_exported(item, url)
  puts '  ok'
end

jsons = (Dir[BACKUP_DIR + '/rates/f*'] + Dir[BACKUP_DIR + '/rates/se*']).map{|f| JSON.load(File.read(f)) }
items = jsons.flat_map{|j| j['user_rates']['content_rated'].each{|i| i['content_type'] = j['user_rates']['current_content']['code'] } }
items.sort_by!{|item| item['rate_date'].to_i }
puts "TOTAL #{items.size}"

export = lambda do
  i = 0
  items.each do |item|
    i += 1
    next if ARGV[0] && i < ARGV[0].to_i
    next if already_exported?(item)
    puts "\n#{i}: #{item['title']} / #{item['year']} / #{item['content_type']} / * #{item['rate']}"
    export_item(item)
    # puts 'Next?'; $stdin.gets
    sleep 30 unless $ask == true
    $ask = false
  end
end
export.call


puts "\nNot Exported"
items.reject{|item| already_exported?(item) }.each{|item| puts "#{item['title']} / #{item['year']} / #{item['content_type']} / * #{item['rate']}" }
