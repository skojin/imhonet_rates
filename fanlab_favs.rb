require 'watir'
require 'nokogiri'

COOKIE_S = ''
COOKIE_ID = ''
BACKUP_DIR = 'json/r'

trap('TERM') { exit }
trap('INT') { exit }

def browser
  $browser ||= create_browser
end

def create_browser
  browser = Watir::Browser.new :chrome

  browser.goto 'https://fantlab.ru/'
  browser.cookies.clear
  browser.cookies.add 'fl_s', COOKIE_S
  browser.cookies.add 'id', COOKIE_ID
  puts 'browser loaded'
  browser
end

$_db = JSON.load(File.read('.db-fanlab-rate.json')) rescue {}
def mark_as_exported(item, url)
  $_db[item['object_id']] = {title: item['title'], url: url, rate: item['rate']}
  File.write('.db-fanlab-rate.json', JSON.dump($_db))
end
def already_exported?(item)
  $_db[item['object_id'].to_s]
end

def export_item(item, with_extra = false)
  title = item['title']
  author = item['collectives'][0]['list'][0]['name']
  browser.goto 'https://fantlab.ru/search-advanced'
  browser.text_field(:id => "ef_name").set title
  if author.count(' ') < 2 # не использовать авторов с отчеством
    browser.text_field(:id => "ef_autor").set author
  end
  unless with_extra
    browser.checkbox(:id => "ef_form_b").click
    browser.checkbox(:id => "ef_form_p").click
    browser.checkbox(:id => "ef_form_m").click
    browser.checkbox(:id => "ef_form_t").click # fary tales
  end

  browser.input(type: 'button').click

  links = browser.div(class: 'search-results').links.select{|x| x.href =~ /work/ }

  if links.size == 0 && with_extra ==  false
    return export_item(item, true)
  end

  if links.size == 0 || links.size > 1
    puts 'SELECT book page in browser and press any button'
    `say select`
    $stdin.gets
    url = browser.url
    puts "  you selected #{url}"
  else
    url = links.first.href
  end

  puts "  #{url}"
  fav!(item, url)
end

def fav!(item, url)
  browser.goto url
  browser.element(id: 'bookcase_panel').click # поставить на полку
  browser.element(id: 'bctext_146186').click # прочитать
  browser.element(id: 'bctext_150066').click # imhonet

  mark_as_exported(item, url)
  puts '  ok'
end

jsons = (Dir[BACKUP_DIR + '/bookmarks/book*']).map{|f| JSON.load(File.read(f)) }
items = jsons.flat_map{|j| j['content']['content']['elements'] }
items.sort_by!{|item| item['id'].to_i }
puts "TOTAL #{items.size}"


export = lambda do
  i = 0
  items.each do |item|
    i += 1
    next if ARGV[0] && i < ARGV[0].to_i
    next if already_exported?(item)
    puts "#{i}: #{item['title']} / #{item['year']} / #{item['collectives'][0]['list'].map{|x| x['name'] }.join(', ')}"
    export_item(item)
    # puts 'Next?'; $stdin.gets
    # sleep 60
  end
end
export.call


puts "\nNot Exported"
items.reject{|item| already_exported?(item) }.each{|item| puts "#{item['title']} / #{item['year']} / #{item['collectives'][0]['list'].map{|x| x['name'] }.join(', ')}" }

