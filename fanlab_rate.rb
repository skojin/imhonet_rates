require 'watir'
require 'nokogiri'

COOKIE_S = ''
COOKIE_ID = ''
BACKUP_DIR = 'json/r'

trap('TERM') { exit }
trap('INT') { exit }

def browser
  $browser ||= create_browser
end

def create_browser
  browser = Watir::Browser.new :chrome

  browser.goto 'https://fantlab.ru/'
  browser.cookies.clear
  browser.cookies.add 'fl_s', COOKIE_S
  browser.cookies.add 'id', COOKIE_ID
  puts 'browser loaded'
  browser
end

$_db = JSON.load(File.read('.db-fanlab-rate.json')) rescue {}
def mark_as_exported(item, url)
  $_db[item['object_id']] = {title: item['title'], url: url, rate: item['rate']}
  File.write('.db-fanlab-rate.json', JSON.dump($_db))
end
def already_exported?(item)
  $_db[item['object_id'].to_s]
end

def export_item(item, with_extra = false)
  title = item['title']
  browser.goto 'https://fantlab.ru/search-advanced'
  browser.text_field(:id => "ef_name").set title
  browser.checkbox(:id => "ef_form_b").click
  browser.checkbox(:id => "ef_form_p").click
  browser.checkbox(:id => "ef_form_m").click
  browser.checkbox(:id => "ef_form_t").click # fary tales
  if with_extra
    browser.checkbox(:id => "ef_form_c").click  # series
    browser.checkbox(:id => "ef_form_g").click # comics
  end

  browser.input(type: 'button').click

  links = browser.div(class: 'search-results').links.select{|x| x.href =~ /work/ }

  if links.size == 0 && with_extra ==  false
    return export_item(item, true)
  end

  if links.size == 0 || links.size > 1
    puts 'SELECT book page in browser and press any button'
    `say select`
    $stdin.gets
    url = browser.url
    puts "  you selected #{url}"
  else
    url = links.first.href
  end

  puts "  #{url}"
  # data = JSON.load a.attribute_value('data-bem')
  # if data['input__suggest-item']['value'] != title
  #   $ask = true
  #   puts "  Title not match: #{data['input__suggest-item']['value']} != #{title}, that's ok?"
  #   puts a.element(class: 'input__suggest-content').text
  #   `say confirm`
  #   if $stdin.gets.chomp != 'y'
  #     return
  #   end
  # end
  rate!(item, url)
end

def rate!(item, url)
  browser.goto url
  browser.element(id: 'bookcase_panel').click # поставить на полку
  browser.element(id: 'bctext_146246').click # прочитал
  browser.element(id: 'bctext_149839').click # imhonet
  # оценка
  browser.element(id: 'main_sel').click
  browser.element(id: "me_#{item['rate']}").click

  mark_as_exported(item, url)
  puts '  ok'
end

jsons = (Dir[BACKUP_DIR + '/rates/book*']).map{|f| JSON.load(File.read(f)) }
items = jsons.flat_map{|j| j['user_rates']['content_rated'].each{|i| i['content_type'] = j['user_rates']['current_content']['code'] } }
items.sort_by!{|item| item['rate_date'].to_i }
puts "TOTAL #{items.size}"

export = lambda do
  i = 0
  items.each do |item|
    i += 1
    next if ARGV[0] && i < ARGV[0].to_i
    next if already_exported?(item)
    puts "\n#{i}: #{item['title']} / #{item['year']} / #{item['content_type']} / * #{item['rate']}"
    export_item(item)
    # puts 'Next?'; $stdin.gets
    # sleep 30 unless $ask == true
    $ask = false
  end
end
# export.call


puts "\nNot Exported"
items.reject{|item| already_exported?(item) }.each{|item| puts "#{item['title']} / #{item['year']} / #{item['content_type']} / * #{item['rate']}" }
