require 'watir'

SESSION_COOKIE = ''
BACKUP_DIR = '' # r

def browser
  $browser ||= create_browser
end

def create_browser
  browser = Watir::Browser.new :chrome

  browser.goto 'https://plus.kinopoisk.ru'
  browser.cookies.clear
  browser.cookies.add 'Session_id', SESSION_COOKIE
  puts 'browser loaded'
  browser
end

$_db = JSON.load(File.read('.db.json')) rescue {}
def mark_as_exported(item, url)
  $_db[item['id']] = {title: item['title'], url: url}
  File.write('.db.json', JSON.dump($_db))
end
def already_exported?(item)
  $_db[item['id'].to_s]
end

def export_item(item)
  title = item['title']
  if title =~ /(.*?)\s\(\d{4}\)$/
    title = $1
    puts "  #{title}"
  end
  browser.text_field(:class => "input__input", name: 'text').set title
  found = browser.div(class: 'input__suggest_visible')
  sleep 2
  if !found.exist?
    puts '  Not Found'
    return
  end
  links = found&.links.select{|x| x.href =~ /film/ }
  a = links.first
  if !a
    puts '  Not Found'
    return
  end

  if links.size > 1
    # puts "  #{links.size} items found, skip"; return
    links.each_with_index do |link, i|
      puts "  #{i + 1}: #{link.element(class: 'input__suggest-content').text}  #{link.href}"
    end
    puts 'SELECT?'
    `say select`
    n = $stdin.gets.chomp.to_i
    if n == 0
      puts '  skip'
      return
    end
    a = links[n - 1]
  end
  url = a.href
  puts "  #{url}"
  data = JSON.load a.attribute_value('data-bem')
  if data['input__suggest-item']['value'] != title
    puts "  Title not match: #{data['input__suggest-item']['value']} != #{title}, that's ok?"
    `say confirm`
    if $stdin.gets.chomp != 'y'
      return
    end
  end
  browser.goto url
  if browser.element(class: "film-actions__favorite-action_checked").exists?
    puts '  Already Exists'
  else
    browser.element(class: "film-actions__favorite-action").click
  end
  mark_as_exported(item, url)
  puts '  ok'
end

jsons = (Dir[BACKUP_DIR + '/bookmarks/f*'] + Dir[BACKUP_DIR + '/bookmarks/se*']).map{|f| JSON.load(File.read(f)) }
items = jsons.flat_map{|j| j['content']['content']['elements'] }

export = lambda do
  i = 0
  items.each do |item|
    i += 1
    next if ARGV[0] && i < ARGV[0].to_i
    next if already_exported?(item)
    puts "#{i}: #{item['title']} / #{item['year']} / #{item['content_type_name']}"
    export_item(item)
    puts 'Next?'; $stdin.gets
    # sleep 60
  end
end
export.call


puts "\nNot Exported"
items.reject{|item| already_exported?(item) }.each{|item| puts "#{item['title']} / #{item['year']} / #{item['content_type_name']}" }
